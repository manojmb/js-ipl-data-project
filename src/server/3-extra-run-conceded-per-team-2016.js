const fs = require('fs');
const { readFileMatches } = require("./read-matches-data");
const csv = require('csv-parser');

const path = "../data/matches.csv";
const outputFolderPath = "../public/output";

// To store delivery.csv values
const deliveries = [];


readFileMatches(path, (err, csvMatchData) => {
    if (err) {
        // Handle errors
        console.error("Error:", err);
        return;
    }

    // csv-parser module is to convert csv to object
    fs.createReadStream('../data/deliveries.csv')
        .pipe(csv())
        .on('data', (data) => deliveries.push(data))
        .on('end', () => {
            try {
                extraRunsConcededPerTeamIn2016(csvMatchData);
            } catch (e) {
                // Catches Error if any and logs it
                console.log(e.message);
            }
        })



});

function extraRunsConcededPerTeamIn2016(csvMatchData) {
    // idOf2016Matches  to store id of 2016 matches
    let idOf2016Matches = []
    // Object extraRunsConcededByTeam to store extra runs conceded by team
    let extraRunsConcededByTeam = Object({});
    // Iterates through csv data
    for (let index = 1; index < csvMatchData.length; index++) {
        // Checks if year matches 2016
        if (parseInt(csvMatchData[index][1]) === 2016) {
            // Pushes id's of match of year 2016
            idOf2016Matches.push(csvMatchData[index][0]);
        }
    }


    for (let deliveryIndex = 0; deliveryIndex < deliveries.length; deliveryIndex++) {
        if (idOf2016Matches.includes(String(deliveries[deliveryIndex].match_id))) {
            extra_runs = parseInt(deliveries[deliveryIndex].extra_runs);
            extraRunsConcededByTeam[deliveries[deliveryIndex].batting_team] = (parseInt(extraRunsConcededByTeam[deliveries[deliveryIndex].batting_team]) || 0) + extra_runs;
        }
    }
    // Blocks program till the data is written and dumps output to output folder
    fs.writeFileSync(`${outputFolderPath}/3-extraRunConcededPerTeamIn2016.json`, JSON.stringify(extraRunsConcededByTeam, null, 2));


}