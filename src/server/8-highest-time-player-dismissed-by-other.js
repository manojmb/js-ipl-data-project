const csv = require('csv-parser');
const fs = require('fs');

const deliveries = [];
const outputFolderPath = "../public/output";



fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (data) => deliveries.push(data))
    .on('end', () => {
        try {
            highestTimePlayerDismissedByOtherPlayer();
        } catch (e) {
            // Catches Error if any and logs it
            console.log(e.message);
        }
    })


function highestTimePlayerDismissedByOtherPlayer() {

    const dismissalCounts = Object({});
    const playerDismissed = [];

    for (let ball = 0; ball < deliveries.length; ball++) {
        if (deliveries[ball].dismissal_kind) {
            playerDismissed.push(`${deliveries[ball].player_dismissed},${deliveries[ball].bowler}`);
        }
    }

    for (let index = 0; index < playerDismissed.length; index++) {
        if (dismissalCounts[playerDismissed[index]] === undefined) {
            dismissalCounts[playerDismissed[index]] = 1;
        }
        else {
            dismissalCounts[playerDismissed[index]] += 1;
        }
    }

    // const dismissalOccurence = highestDismissal("DA Warner", "BW Hilfenhaus", dismissalCounts);

    maxValue = Math.max(...Object.values(dismissalCounts))

    const highestDismissalCount = [];
    for (let key in dismissalCounts) {
        if (maxValue === dismissalCounts[key]) {
            highestDismissalCount.push(highestDismissal(key.split(',')[0], key.split(',')[1], dismissalCounts))
        }
    }

    // const dismissalOccurence = highestDismissal("RG Sharma", "DJ Bravo", dismissalCounts);
    fs.writeFileSync(`${outputFolderPath}/8-highestTimePlayerDismissedByOtherPlayer.json`, JSON.stringify(highestDismissalCount, null, 2));
}

function highestDismissal(playerDismissed, dismissedby, dismissalCounts) {
    const dismissal = Object({});
    dismissal['dismissedby'] = dismissedby;
    dismissal['dismissed'] = playerDismissed;
    dismissal["number of times"] = dismissalCounts[`${playerDismissed},${dismissedby}`];
    return dismissal;

}