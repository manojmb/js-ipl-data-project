const fs = require('fs');
const { readFileMatches } = require("./read-matches-data");

const path = "../data/matches.csv";
const outputFolderPath = "../public/output";

readFileMatches(path, (err, csvMatchData) => {
    if (err) {
        // Handle errors
        console.error("Error:", err);
        return;
    }

    try {
        totalMatchesPerYear(csvMatchData);
    } catch (e) {
        // Catches Error if any and logs it
        console.log(e.message);
    }

});

function totalMatchesPerYear(csvMatchData) {
    // Object totalMatches stores total matches per year
    let totalMatches = Object({})
    // Iterates through csv data except heading
    for (let index = 1; index < csvMatchData.length; index++) {
        // Checks if the match is saved or not
        if (totalMatches[csvMatchData[index][1]] === undefined) {
            // If not creates match of year and assumes it as first match
            totalMatches[csvMatchData[index][1]] = 1;
        } else {
            // Increment match count
            totalMatches[csvMatchData[index][1]] += 1;

        }
    }
    // Blocks program till the data is written and dumps output to output folder
    fs.writeFileSync(`${outputFolderPath}/1-matchesPerYear.json`, JSON.stringify(totalMatches, null, 2));

}