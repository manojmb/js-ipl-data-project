const fs = require("fs");


// Function to read the file and invoke a callback with the data
const readFileMatches = (filePath, callback) => {
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error("Error while reading:", err);
      callback(err, null);
      return;
    }

    // Split the data into lines
    const lines = data.split("\n");

    // Initialize the output array
    const output = lines.map((line) => line.split(","));

    // Pass the output to the callback
    callback(null, output);
  });
};
module.exports = { readFileMatches };