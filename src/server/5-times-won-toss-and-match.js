const fs = require('fs');
const { readFileMatches } = require("./read-matches-data");

const path = "../data/matches.csv";
const outputFolderPath = "../public/output";

readFileMatches(path, (err, csvMatchData) => {
    if (err) {
        // Handle errors
        console.error("Error:", err);
        return;
    }
    try {
        numberOfTimesWonMatchAndToss(csvMatchData);
    } catch (e) {
        // Catches Error if any and logs it
        console.log(e.message);
    }

});

function numberOfTimesWonMatchAndToss(csvMatchData) {
    // Object to store toss and match winner record
    const wonTossAndMatches = Object({});
    for (let index = 1; index < csvMatchData.length; index++) {
        // Winner of toss
        const tossWinner = csvMatchData[index][6];
        // Winner of match
        const matchWinner = csvMatchData[index][10];

        if (tossWinner === matchWinner) {
            wonTossAndMatches[tossWinner] = (wonTossAndMatches[tossWinner] || 0) + 1;
        }
    }
    fs.writeFileSync(`${outputFolderPath}/5-timesTeamWonTossAndMatch.json`, JSON.stringify(wonTossAndMatches, null, 2));

}