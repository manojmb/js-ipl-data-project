const csv = require('csv-parser');
const fs = require('fs');

const deliveries = [];
const outputFolderPath = "../public/output";




fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (data) => deliveries.push(data))
    .on('end', () => {
        try {
            bestBowlerEconomySuperOver();
        } catch (e) {
            // Catches Error if any and logs it
            console.log(e.message);
        }
    })



function bestBowlerEconomySuperOver() {

    let bowler_stats = {};
    for (let index = 0; index < deliveries.length; index++) {
        const totalRuns = deliveries[index].total_runs;
        const bowler = deliveries[index].bowler;
        if (deliveries[index].is_super_over != '0') {
            if (bowler_stats[bowler] === undefined) {
                bowler_stats[bowler] = {
                    "runs": parseInt(totalRuns),
                    "balls": 1
                };
            } else {
                bowler_stats[bowler].runs += parseInt(totalRuns);
                bowler_stats[bowler].balls += 1;
            }
        }

    }
    for (let bowler in bowler_stats) {
        bowler_stats[bowler] = calculateEconomyRate(bowler_stats[bowler].runs, parseInt(bowler_stats[bowler].balls));
    }


    const sortedBowlers = Object.keys(bowler_stats);
    const length = sortedBowlers.length;

    for (let aIndex = 0; aIndex < length - 1; aIndex++) {
        for (let bIndex = 0; bIndex < length - aIndex - 1; bIndex++) {
            const bowlerA = sortedBowlers[bIndex];
            const bowlerB = sortedBowlers[bIndex + 1];

            if (parseFloat(bowler_stats[bowlerA]) > parseFloat(bowler_stats[bowlerB])) {
                // Swap bowler positions
                [sortedBowlers[bIndex], sortedBowlers[bIndex + 1]] = [sortedBowlers[bIndex + 1], sortedBowlers[bIndex]];
            }
        }
    }


    const bestEconomyinSuperOver = sortedBowlers.slice(0, 1);
    const bestEconomyinSuperOverData = Object({});
    bestEconomyinSuperOverData[bestEconomyinSuperOver[0]] = bowler_stats[bestEconomyinSuperOver[0]];
    fs.writeFileSync(`${outputFolderPath}/9-bestBowlerEconomyInSuperOver.json`, JSON.stringify(bestEconomyinSuperOverData, null, 2));


}

function calculateEconomyRate(runsConceded, oversBowled) {
    return (runsConceded / (oversBowled / 6)).toFixed(2);
}