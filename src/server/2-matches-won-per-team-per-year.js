const fs = require('fs');
const { readFileMatches } = require("./read-matches-data");

const path = "../data/matches.csv";
const outputFolderPath = "../public/output";

readFileMatches(path, (err, csvMatchData) => {
    if (err) {
        // Handle errors
        console.error("Error:", err);
        return;
    }
    try {
        matchesWonPerTeamperYear(csvMatchData);
    } catch (e) {
        // Catches Error if any and logs it
        console.log(e.message);
    }

});

function matchesWonPerTeamperYear(csvMatchData) {
    // Object totalMatchesWonPerTeam to store matches won per team
    let totalMatchesWonPerTeam = Object({});

    // Iterates through csv data
    for (let index = 1; index < csvMatchData.length; index++) {
        const year = csvMatchData[index][1];
        const winner = csvMatchData[index][10];

        // Initialize year if not present
        if (totalMatchesWonPerTeam[year] === undefined) {
            totalMatchesWonPerTeam[year] = {};
        }

        // Increment match count for each team
        totalMatchesWonPerTeam[year][winner] = (totalMatchesWonPerTeam[year][winner] || 0) + 1;
    }

    // Blocks program till the data is written and dumps output to output folder
    fs.writeFileSync(`${outputFolderPath}/2-matchesWonPerTeamPerYear.json`, JSON.stringify(totalMatchesWonPerTeam, null, 2));
}