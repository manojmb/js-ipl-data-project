const csv = require('csv-parser');
const fs = require('fs');
const { readFileMatches } = require("./read-matches-data");

const deliveries = [];
const path = "../data/matches.csv";
const outputFolderPath = "../public/output";


readFileMatches(path, (err, csvMatchData) => {
    if (err) {
        // Handle errors
        console.error("Error:", err);
        return;
    }

    fs.createReadStream('../data/deliveries.csv')
        .pipe(csv())
        .on('data', (data) => deliveries.push(data))
        .on('end', () => {
            try {
                top10EconomicBowlerOf2015(csvMatchData);
            } catch (e) {
                // Catches Error if any and logs it
                console.log(e.message);
            }
        })


});

function top10EconomicBowlerOf2015(csvMatchData) {
    // Array to store id's of 2015 matches
    const idOf2015Matches = [];
    for (let index = 1; index < csvMatchData.length; index++) {
        if (parseInt(csvMatchData[index][1]) === 2015) {
            idOf2015Matches.push(csvMatchData[index][0]);
        }
    }

    // Object to store every bowlers balls and runs conceded
    const bowler_stats = {};

    for (let index = 0; index < deliveries.length; index++) {
        if (idOf2015Matches.includes(deliveries[index].match_id)) {
            const eachBall = deliveries[index];
            const totalRuns = eachBall.total_runs;
            const bowler = eachBall.bowler;
            if (bowler_stats[bowler] === undefined) {
                bowler_stats[bowler] = {
                    "runs": parseInt(totalRuns),
                    "balls": 1
                };
            } else {
                bowler_stats[bowler].runs += parseInt(totalRuns);
                bowler_stats[bowler].balls += 1;
            }
        }

    }
    for (let bowler in bowler_stats) {
        bowler_stats[bowler]['economy'] = calculateEconomyRate(bowler_stats[bowler].runs, parseInt(bowler_stats[bowler].balls));
    }

    let sortedBowlers = Object.keys(bowler_stats);
    const length = sortedBowlers.length;

    for (let aIndex = 0; aIndex < length - 1; aIndex++) {
        for (let bIndex = 0; bIndex < length - aIndex - 1; bIndex++) {
            const bowlerA = sortedBowlers[bIndex];
            const bowlerB = sortedBowlers[bIndex + 1];

            if (parseFloat(bowler_stats[bowlerA]['economy']) > parseFloat(bowler_stats[bowlerB]['economy'])) {
                // Swap bowler positions
                [sortedBowlers[bIndex], sortedBowlers[bIndex + 1]] = [sortedBowlers[bIndex + 1], sortedBowlers[bIndex]];
            }
        }
    }

    // Get the top 10 economical bowlers
    sortedBowlers = sortedBowlers.slice(0, 10);
    const top10Bowlers = Object({});
    for (let bowler of sortedBowlers) {
        top10Bowlers[bowler] = bowler_stats[bowler];
    }
    fs.writeFileSync(`${outputFolderPath}/4-top10EconomicPlayerof2015.json`, JSON.stringify(top10Bowlers, null, 2));
}

function calculateEconomyRate(runsConceded, oversBowled) {
    return (runsConceded / (oversBowled / 6)).toFixed(2);
}