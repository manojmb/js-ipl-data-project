const fs = require('fs');
const { readFileMatches } = require("./read-matches-data");

const path = "../data/matches.csv";
const outputFolderPath = "../public/output";

readFileMatches(path, (err, csvMatchData) => {
    if (err) {
        // Handle errors
        console.error("Error:", err);
        return;
    }
    try {
        highestPlayerOfMatchPerSeason(csvMatchData);
    } catch (e) {
        // Catches Error if any and logs it
        console.log(e.message);
    }

});



function highestPlayerOfMatchPerSeason(csvMatchData) {
    // Object to store player of match record
    const playerOfMatchRecord = Object({});
    for (let index = 1; index < csvMatchData.length; index++) {
        const player = csvMatchData[index][13];
        const year = csvMatchData[index][1];
        // If no result
        if (player) {
            if (playerOfMatchRecord[year] === undefined) {
                playerOfMatchRecord[year] = {};
            }
            if (playerOfMatchRecord[csvMatchData[index][1]][player] === undefined) {
                playerOfMatchRecord[csvMatchData[index][1]][player] = 1;
            } else {
                playerOfMatchRecord[csvMatchData[index][1]][player] += 1;
            }
        }
    }



    const highestPlayerOfMatchRecord = Object({});



    for (let year in playerOfMatchRecord) {
        let maxValue = Math.max(...Object.values(playerOfMatchRecord[year]));
        if (highestPlayerOfMatchRecord[year] === undefined) {
            highestPlayerOfMatchRecord[year] = {};
        }
        for (let player in playerOfMatchRecord[year]) {
            if (playerOfMatchRecord[year][player] === maxValue) {
                highestPlayerOfMatchRecord[year][player] = playerOfMatchRecord[year][player];
            }

        }
    }
    fs.writeFileSync(`${outputFolderPath}/6-highestPlayerOfMatchRecord.json`, JSON.stringify(highestPlayerOfMatchRecord, null, 2));

}