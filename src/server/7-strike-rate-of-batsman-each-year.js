const csv = require('csv-parser')
const fs = require('fs')
const { readFileMatches } = require("./read-matches-data");

const path = "../data/matches.csv";
const outputFolderPath = "../public/output";

const deliveries = [];

readFileMatches(path, (err, csvMatchData) => {
    if (err) {
        // Handle errors
        console.error("Error:", err);
        return;
    }

    fs.createReadStream('../data/deliveries.csv')
        .pipe(csv())
        .on('data', (data) => deliveries.push(data))
        .on('end', () => {
            try {
                strikeRateOfBatsmanEachYear(csvMatchData);
            } catch (e) {
                // Catches Error if any and logs it
                console.log(e.message);
            }
        })


});

function strikeRateOfBatsmanEachYear(csvMatchData) {
    const matchIDandYear = Object({});
    for (let index = 1; index < csvMatchData.length; index++) {
        if (matchIDandYear[csvMatchData[index][1]] === undefined) {
            matchIDandYear[csvMatchData[index][1]] = [csvMatchData[index][0]];
        } else {
            matchIDandYear[csvMatchData[index][1]].push(csvMatchData[index][0]);
        }
    }


    const matchRecord = Object({});
    for (let ball = 0; ball < deliveries.length; ball++) {
        for (let matchYear in matchIDandYear) {
            if (matchIDandYear[matchYear].includes(deliveries[ball].match_id)) {
                year = matchYear;
                break;
            }

        }
        if (matchRecord[year] === undefined) {
            matchRecord[year] = {};
        }
        if (matchRecord[year][deliveries[ball].batsman] === undefined) {
            matchRecord[year][deliveries[ball].batsman] = {};
        }

        let runs = parseInt(matchRecord[year][deliveries[ball].batsman].runs);
        let balls = parseInt(matchRecord[year][deliveries[ball].batsman].balls);
        matchRecord[year][deliveries[ball].batsman].runs = (runs || 0) + parseInt(deliveries[ball].batsman_runs);
        matchRecord[year][deliveries[ball].batsman].balls = (balls || 0) + 1;
    }

    for (let keys in matchRecord) {
        for (let batsman in matchRecord[keys]) {
            matchRecord[keys][batsman]['StrikeRate'] = calculateStrikeRate(matchRecord[keys][batsman]['runs'], matchRecord[keys][batsman]['balls']);
        }
    }

    // const strikeRateOfBatsman = strikeRate("BB McCullum", matchRecord);
    fs.writeFileSync(`${outputFolderPath}/7-strikeRateOfBatsmanEachYear.json`, JSON.stringify(matchRecord, null, 2));
}


// To find strike rate of individual batsman by name
function strikeRate(name, matchRecord) {
    const particularPlayerRecord = Object({});
    for (let year in matchRecord) {
        particularPlayerRecord[year] = {};
        for (let player in matchRecord[year]) {
            if ([player === name]) {
                particularPlayerRecord[year][name] = matchRecord[year][name];
            }
        }
    }
    return particularPlayerRecord;
}

function calculateStrikeRate(runsScored, ballsFaced) {
    return parseInt(((runsScored / ballsFaced) * 100).toFixed(2));
}